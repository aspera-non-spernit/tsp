extern crate genetics;
extern crate rand;
use rand::{ Rng };
use std::collections::{HashSet};

use genetics::utils::{ city, cities, population_fitness, routes, total_fitness, vec_print };

const K: usize = 10;


/** Aufgabe 1
 **/
 fn random_chromosome() -> Vec<usize> {
   //... Deine Immplementierung hier (ca. 7 Zeilen)
}


/** Aufgabe 3
 * Evaluiert die gefahrenen Strecken.
 */
pub fn driven(chromosome: &[usize]) -> usize {
    // Deine Implementierung hier (ca. 23 Zeilen, geht auch mit weniger)
}

/** 
 * Aufgabe 5
 * */
fn mating_pool(evaluated: Vec<(Vec<usize>, usize)>) -> Vec <( Vec<usize>, usize) > {
    // Deine Implementierung hier (1 Zeile bei funkionaler Programmierung, sonst ca. 5 Zeilen)
}

/**
 * Aufgabe 6
 **/
fn biased_roulette_wheel(mating_pool: &[(Vec<usize>, usize)]) -> Vec<Vec<usize>> {
    // Deine Implementierung hier (1 Zeile bei funkionaler Programmierung, sonst ca. 16 Zeilen)

}


/**
 * Aufgabe 7
 **/
fn mutate(chromsomes: &mut[Vec<usize>]) -> &mut[Vec<usize>] {
    // Deine Implementierung hier (1 Zeile bei funkionaler Programmierung, sonst ca. 11 Zeilen)

}


/**
 * Aufgabe 8
 **/
 fn selection() {

}

fn main() {
    // Aufgabe 2 
    // Deine Implementierung hier (ca. 4 Zeilen)


    // Vom Lehrer vorgegeben
    println!("Die Population besteht aus folgenden Chromosomen.");
    vec_print(&population);

    // Vom Lehrer vorgegeben
    println!("Die Chromosome nach Ihrer Fitness sortiert.");
    // Aufgabe 4
    // Deine Implenentierung hier (1 Zeile)

    // Vom Lehrer vorgegeben
    vec_print(&evld);
   
    // Vom Lehrer vorgegeben
    println!("Die fittesten Chromosome kommen in den Mating-Pool:");
    let mp = mating_pool(evld.to_vec()); // why reference from pop_fit?
    vec_print(&mp);
   
    // Vom Lehrer vorgegeben
    let mut brw = biased_roulette_wheel(&mp);
    println!("Das Biased-Roulette-Wheel");
    vec_print(&brw);

    // Aufgabe 9
}