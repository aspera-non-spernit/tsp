# Travelling Sales Person Problem

Eine einfache Implementierung eines Genetischen Algorithmus
zum Lösen des Travelling Sales Person Problem

Dieses Programm wurde für eine Projektarbeit der Vorlesung "Didaktik der Informatik"
im Wintersemester 2020/21 an der Universität Potsdam erstellt. 
Es dient als praktische Übung / Unterstützung für den Informatikunterricht
im Themenfeld "Evolutionäre Algorithmen".


## Städte und Entfernungen

wego.here.com Auto erste Auswahl

| Strecke  | Berlin | Hamburg | Dortmund | Dresden | München |         |
|:--------:|:------:|:-------:|:--------:|:-------:|:-------:|:-------:|
| Berlin   | 0      | 286     | 490      | 192     | 565     | ..      |
| Hamburg  | 286    | 0       | 353      | 509     | 761     | ..      |
| Dortmund | 490    | 353     | 0        | 554     | 613     | ..      |
| Dresden  | 192    | 509     | 554      | 0       | 451     | ..      |
| München  | 565    | 761     | 613      | 451     | 0       | ..      |
| ..       | ..     | ..      | ..       | ..      | ..      | 0       |


## /examples Ordner

Im "examples" Order befinden sich keine lauffähigen Beispiele, sondern die Aufgaben,
die von Schülerinnen und Schülern implementiert werden sollen.


## /src/lib/utils Order

Im "/src/lib/utils" Order befinden sich Hilfsfunktionen, die nicht zu den Kernfunktionen Genetischer Algorithmen
gehören, und von den Schülern genutzt werden können, um sie bei der Implementierung zu unterstützen.

## Aufgabenblatt

1. Implementiere die Funktion ```random_chromosome() -> Vec<usize>```.

Die Funktion benötigt keine Argumente.
Der zurückzugebene Vektor V mit N zufälligen Zahlen aus der Zahlenmenge {1..C}.  
C ist die Anzahl der Städte. Den Wert kannst Du durch enum City{ … } oder aus ```cities() -> Vec<(usize, City)>``` ableiten.
Die Länge N des Vektors V ist für das Problemm des Handlungsreisenden gleich der Länge C.

Zeit übrig?: Stelle sicher, dass jedes generierte Chromosom einzigartig ist (keine Zwillinge).

1. Rufe die Funktion random_chromsome aus der Funktion main so oft auf, dass Du eine Population P von K Chromosomen generierst.
Packe die Chromosome in einen zweidimensionalen Vektor mut population: ```Vec<Vec<usize>>```

1. Implementiere die Funktion ```driven(chromosome: &[usize]) -> usize```.
 Die Funktion zählt die gefahrenen km eines Chromosoms zusammen und gibt den Wert zurück.

1. Rufe die Funktion ```population_fitness(population: &[Vec<usize>]) -> Vec<(Vec<usize>, usize)>``` auf und speichere die Rückgabe in der Variable evld

1. Implementiere die Funktion ```mating_pool(evaluated: Vec<(Vec<usize>, usize)>) -> Vec <( Vec<usize>, usize) >```

Die Funktion nimmt als Argument den zweidimensionalen Vektor den du als population gespeichert hast und gebe einen neuen zurück, welcher eine Untermenge von population ist. Stelle sicher, dass nur die besten Chromosome zurückgegeben werden.  

1. Implementiere eine Funktion ```biased_roulette_wheel(mating_pool: &[(Vec<usize>, usize)]) -> Vec<Vec<usize>>```  

1. Führe das Programm aus. Betrachte das Ergebnis der Funktion biased_roulette_wheel.

Erläutere, ob und warum Deine Implementierung besonders geeignet / nicht geeignet ist für dieses spezielle Problem des TSP (mit den gegebenen Städten).
Gebe einen Vektor V einer Länge zurück, die dedem Wert aus der Funktion ```total_fitness(mating_pool: &[(Vec<usize>, usize)]) -> usize```