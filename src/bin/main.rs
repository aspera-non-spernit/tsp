extern crate tsp;
extern crate rand;

use rand::{ Rng };
use std::collections::{ HashSet };

use tsp::{ io::{GoogleMapsLink, History, check_and_log}, utils::{ city, cities, population_fitness, routes, total_fitness } };

const K: usize = 20; // Bedingung K und K / 2 muss gerade sein.


/** Aufgabe 1
 * Zu Beginn wird eine Population von k Chromosomen generiert.
 * Um zu garantieren, dass jede Stadt einmal besucht wird,
 * entspricht ein Gen auf dem Chromosom einer Stadt.
 * Die Abfolge der Gene auf dem Chromosom entspricht der gefahrenen
 * Strecken.
 * Dies macht Sinn, wenn in der Population größe unterschiede in der Fitness herrschen
 * Hier nicht so sehr.
 **/
 fn random_chromosome() -> Vec<usize> {
    let mut ch: HashSet<usize> = HashSet::new();
    let mut rng = rand::thread_rng();
    while ch.len() != cities().len() {
        ch.insert( rng.gen_range(1..=cities().len()) );
    }
    let chromosome: Vec<usize> = ch.into_iter().collect();
    chromosome
}


/** Aufgabe 3
 * Evaluiert die gefahrenen Strecken.
 */
pub fn driven(chromosome: &[usize]) -> usize {
    let routes = routes();
    let mut distance = 0usize;
    for (i, gene) in chromosome.iter().enumerate() {
        let from =  city(*gene);
        if i < chromosome.len() - 1 {
            let to = city(chromosome[i + 1]);
            for c in &routes {
                if c.0 == from && c.1 == to {
                    let current_dist = c.2;
                    distance += current_dist;
                }
            }
        } else {
            let to = city(chromosome[0]);
            for c in &routes {
                if c.0 == from && c.1 == to {
                    let current_dist = c.2;
                    distance += current_dist;
                }
            }
        }      
    }
    distance
}

/** 
 * Aufgabe 5
 * Wählt die vier stärksten Chromosome aus
 * */
fn mating_pool(evaluated: &[(Vec<usize>, usize)]) -> Vec<Vec<usize>> {
    evaluated.iter()
        .cloned()
        .take(4)
        .collect::<Vec<(Vec<usize>, usize)>>()
        .iter()
        .cloned()
        .map(|c| {c.0} )
        .collect()
}

/**
 * Aufgabe 6
 * Nimmt eine nach Fitness sortierte Population 
 * und gibt füllt ein BRW mit Paarungskanditen
 * das nach
 **/
fn biased_roulette_wheel(evaluated: &[(Vec<usize>, usize)]) -> Vec<Vec<usize>> {
    let total_fitness = total_fitness(evaluated);
    let mut pool: Vec<(Vec<usize>, f32)> = vec![];
    let mut total_weight = 0f32;
    for chromosome in evaluated {
        let weight = (100 - chromosome.1 * 100 / total_fitness) as f32;
        pool.push( (chromosome.0.clone(), weight) );    
        total_weight += weight;
    }
    let mut brw = vec![];
    for c in pool {
        let weight = (c.1 as f32 * 100f32 / total_weight).round() as usize;
        for _ in 0..weight {
            brw.push(c.0.clone());
        }
    }
    brw
}

/**
 * Aufgabe 7
 * Sucht sich zwei zufällige Gene auf dem Chromosom aus, 
 * und vertauscht sie.
 * Die Chance für eine Mutation ist 1/6
 * Wenn zufällig zwei mal die gleiche Zahl generiert wird,
 * findet keine Mmutation statt.
 **/
fn mutate(chromsomes: &mut[Vec<usize>]) -> &mut[Vec<usize>] {
    let mut rng = rand::thread_rng();
    let mutated_chromosome = rng.gen_range(0..chromsomes.len());
    let first = rng.gen_range(0..cities().len());
    let second = rng.gen_range(0..cities().len());
    if first != second {
     //   println!("Chromosom {:?} mutierte", chromsomes[mutated_chromosome] );
        chromsomes[mutated_chromosome].swap(first, second);
     //   println!("zu        {:?}.", chromsomes[mutated_chromosome]);
    }
    chromsomes
}

/**
 * Aufgabe 8
 * 
 */
fn offspring(population: &[Vec<usize>]) -> Vec<Vec<usize>> {
    let mut rng = rand::thread_rng();
   
    let chromosome_1 = population[ rng.gen_range(0..population.len() - 1) ].clone();
    let chromosome_2 = population[ rng.gen_range(0..population.len() - 1) ].clone();

    let mut parts_1: (Vec<usize>, Vec<usize>) = ( chromosome_1.get(0..=2).unwrap().to_vec(), chromosome_1.get(3..chromosome_1.len() ).unwrap().to_vec() );
    let mut parts_2: (Vec<usize>, Vec<usize>) = ( chromosome_2.get(0..=2).unwrap().to_vec(), chromosome_2.get(3..chromosome_2.len() ).unwrap().to_vec() );
   
    let mut exists_at = vec![];
    let mut offspring_1 = vec![];
    let mut offspring_2 = vec![];
    
    for g1 in &parts_1.0 {
        for (i, g2) in chromosome_2.iter().enumerate() {
            if g1 == g2 {
                exists_at.push(i);
            }
        }
    }
    for (i, g2) in chromosome_2.iter().enumerate() {
        if !exists_at.contains(&i) {
            offspring_2.push(*g2);
        }
    }

    exists_at = vec![];
    for g2 in &parts_2.1 {
        for (i, g1) in chromosome_1.iter().enumerate() {
            if g1 == g2 {
                exists_at.push(i);
            }
        }
    }
    for (i, g1) in chromosome_1.iter().enumerate() {
        if !exists_at.contains(&i) {
            offspring_1.push(*g1);
        }
    }

    offspring_1.append(&mut parts_2.1);
    offspring_2.append(&mut parts_1.0);

    vec![offspring_1, offspring_2]
}


fn main() -> std::io::Result<()> {
    let mut history = History { lowest: None, fittest: vec![] };

    // Aufgabe 2 
    let mut population: Vec<Vec<usize>> = vec![];
    while population.len() != K {
        population.push( random_chromosome() );
    }

    let mut generation = 0;

    loop {
      //  std::thread::sleep(std::time::Duration::from_millis(50));
   
        // Aufgabe 4
        let evld = &population_fitness(&population);
      
        // Vom Lehrer vorgegeben
        let gml: GoogleMapsLink = evld[0].0.clone().into();
        match history.lowest {
            Some(fittest) => {
                if evld[0].1 < fittest {
                    history.lowest = Some(evld[0].1)
                }
            },
            None => { history.lowest = Some(evld[0].1); }
        }

        history.fittest.push( (generation, evld[0].0.clone(), evld[0].1, gml.link ));   
        
        match check_and_log(&history) {
            Ok(()) => {  /*println!("Historie gespeichert.")*/ },
            Err(e) => { println!("{:?}", e) }
        }
        
        // Zwei alternative Selektionsalgorithmen
        
        // 1. probabilistische Elternselektion
        // let mut mp = biased_roulette_wheel(&evld);

        // 2. Survival of the Fittest (Elitismus)
        let mut mp = mating_pool(&evld);

        // Vom Lehrer vorgegeben
        mp = mutate(&mut mp).to_vec();

        let mut new_population = vec![];
        for _ in 0..(K / 2) {
            new_population.append(&mut offspring(&mp));
        }
        population = new_population;
           


        generation += 1;
        if generation == 10001 { break; }
    }

    // Vom Lehrer vorgegeben
    // println!("Die fittesten Chromosome kommen in den Mating-Pool:");
    // let mp = mating_pool(evld.to_vec()); // why reference from pop_fit?
    // vec_print(&mp);

    Ok(())
}