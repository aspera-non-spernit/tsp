use std::io::Write;
use crate::utils;

/** Generation, Chromosom, fitness **/
#[derive(Debug)]
pub struct History { pub lowest: Option<usize>, pub fittest: Vec< (usize, Vec<usize>, usize, String) > } // Generation fitness

#[derive(Debug)]
pub struct GoogleMapsLink { pub link: String }

impl History {
    pub fn save(&self) -> std::io::Result<()> {
        let mut f = std::fs::File::create("history.csv")?;
        f.write_fmt(format_args!("{}", self))?;
        Ok(())
    }
}

impl std::fmt::Display for History {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Generation; Fitness; Chromosom; Link")?;
        for e in self.fittest.iter() {
            write!(f, "{}; {}; {:?}; {}", e.0,  e.2, e.1, e.3)?;
            writeln!(f, )?;
        }
        std::fmt::Result::Ok(())
    }
}

impl From<Vec<usize>> for GoogleMapsLink {
    fn from(route: Vec<usize>) -> GoogleMapsLink {
        let mut link = "https://www.google.com/maps/dir".to_string();
        let cities = utils::cities();
        for g in &route {
            for c in &cities {
                if c.0 == *g {
                    link.push('/');
                    link.push_str(&c.2);
                }
            }
        }
        for c in &cities {
            if c.0 == route[0] {        
                link.push('/');
                link.push_str(&c.2);
            }
        }
        GoogleMapsLink { link }

    }

}


pub fn check_and_log(history: &History) -> std::io::Result<()> {
    // Fitteste Chromosom ausgeben und History speichern, wenn sich die Fitness verbessert hat
    if history.fittest.len() > 1 {
        match history.lowest {
            Some(lowest) => {

                if history.fittest[history.fittest.len() - 2].2 > lowest {
                    println!("Generation {:?} brachte eine neue kürzeste Strecke hervor: {:?}km", history.fittest[history.fittest.len() - 2].0, history.fittest[history.fittest.len() - 2].2);
                    println!("{:?}", history.fittest[history.fittest.len() - 2].3 );
                    history.save()?;
                }
            },
            None => {}
        }  
    } else {
        println!("Das fitteste Chromosom der ersten Generation hat eine Fitness von {:?}km", history.fittest[history.fittest.len() - 1].2);
    }

    Ok(())
}