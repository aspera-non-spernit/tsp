

#[derive(Clone, Debug, PartialEq)]
pub enum City { Basel, Berlin, Dortmund, Dresden, Frankfurt, Hamburg, Muenchen, Potsdam, Strassburg }


pub fn city(gene: usize) -> City {
    let cities = cities();
    let mut city = City::Berlin;
    for c in &cities {
        if c.0 == gene {
     
            city = c.1.clone();
        }
    }
    city
}

pub fn vec_print<T: std::fmt::Debug>(vec: &[T]) {
    for v in vec {
        println!("{:?}", v);
    }
}

/***
 * Sortiert die Population nach Fitness
 **/
pub fn population_fitness(population: &[Vec<usize>]) -> Vec<(Vec<usize>, usize)> {
    let mut evld: Vec<(Vec<usize>, usize)> = population.iter()
    .map(|d| {   
        (d.clone(), driven(d) )
    }).collect();

    evld.sort_by(|a, b| {
        a.1.cmp(&b.1)
    });
    evld
}

pub fn total_fitness(mating_pool: &[(Vec<usize>, usize)]) -> usize {
    mating_pool.iter().fold(0usize, |acc, x| acc + x.1 as usize)
}
/***
 * Die Strecken zwischen den Städten sind die "Gene"
 * in unserem Programmm. Das Programm kann also aus
 * 20 verschiedenen Genen bestehen. Jedoch nur 6
 * Gene werden benötigt, damit alle Städte mindestens
 * einmal besucht werden. 7 Gene, damit der Reisende
 * wieder am Ursprungsort ankommt.
 * Nur eine Kombination der 7 Gene ist die optimalste Lösung.
 * Die optimalste Reise ist die kürzeste Reise.
 **/

pub fn routes() -> Vec<(City, City, usize)> {
    vec![
        (City::Basel, City::Berlin, 867),
        (City::Basel, City::Dortmund, 544),
        (City::Basel, City::Dresden, 740),
        (City::Basel, City::Frankfurt, 340),
        (City::Basel, City::Hamburg, 815),
        (City::Basel, City::Muenchen, 408),
        (City::Basel, City::Potsdam, 813),
        (City::Basel, City::Strassburg, 139),

        (City::Berlin, City::Basel, 867),
        (City::Berlin, City::Dortmund, 490),
        (City::Berlin, City::Dresden, 192),
        (City::Berlin, City::Frankfurt, 543),
        (City::Berlin, City::Hamburg, 286),
        (City::Berlin, City::Muenchen, 565),
        (City::Berlin, City::Potsdam, 36),
        (City::Berlin, City::Strassburg, 755),

        (City::Dortmund, City::Basel, 544),
        (City::Dortmund, City::Berlin, 490),
        (City::Dortmund, City::Dresden, 554),
        (City::Dortmund, City::Frankfurt, 222),
        (City::Dortmund, City::Hamburg, 353),
        (City::Dortmund, City::Muenchen, 613),
        (City::Dortmund, City::Potsdam, 460),
        (City::Dortmund, City::Strassburg, 432),

        (City::Dresden, City::Basel, 740),
        (City::Dresden, City::Berlin, 192),
        (City::Dresden, City::Dortmund, 554),
        (City::Dresden, City::Frankfurt, 460),
        (City::Dresden, City::Hamburg, 509),
        (City::Dresden, City::Muenchen, 451),
        (City::Dresden, City::Potsdam, 205),
        (City::Dresden, City::Strassburg, 638),

        (City::Hamburg, City::Basel, 815),
        (City::Hamburg, City::Berlin, 286),
        (City::Hamburg, City::Dortmund, 353),
        (City::Hamburg, City::Dresden, 509),
        (City::Hamburg, City::Frankfurt, 493),
        (City::Hamburg, City::Muenchen, 761),
        (City::Hamburg, City::Potsdam, 289),
        (City::Hamburg, City::Strassburg, 704),

        (City::Muenchen, City::Basel, 408),
        (City::Muenchen, City::Berlin, 565),
        (City::Muenchen, City::Dortmund, 613),
        (City::Muenchen, City::Dresden, 451),
        (City::Muenchen, City::Frankfurt, 376),
        (City::Muenchen, City::Hamburg, 761),
        (City::Muenchen, City::Potsdam, 446),
        (City::Muenchen, City::Strassburg, 368),

        (City::Potsdam, City::Basel, 813),
        (City::Potsdam, City::Berlin, 36),
        (City::Potsdam, City::Dortmund, 460),
        (City::Potsdam, City::Dresden, 289),
        (City::Potsdam, City::Frankfurt, 477),
        (City::Potsdam, City::Hamburg, 205),
        (City::Potsdam, City::Muenchen, 446),
        (City::Potsdam, City::Strassburg, 698),

        (City::Frankfurt, City::Basel, 340),
        (City::Frankfurt, City::Berlin, 543),
        (City::Frankfurt, City::Dortmund, 222),
        (City::Frankfurt, City::Dresden, 460),
        (City::Frankfurt, City::Hamburg, 493),
        (City::Frankfurt, City::Muenchen, 376),
        (City::Frankfurt, City::Potsdam, 477),
        (City::Frankfurt, City::Strassburg, 217),

        (City::Strassburg, City::Basel, 139),
        (City::Strassburg, City::Berlin, 755),
        (City::Strassburg, City::Dortmund, 432),
        (City::Strassburg, City::Dresden, 638),
        (City::Strassburg, City::Frankfurt, 217),
        (City::Strassburg, City::Hamburg, 704),
        (City::Strassburg, City::Muenchen, 368),
        (City::Strassburg, City::Potsdam, 698),


    ]
}

/**
 * Übung hinzufügen von Städten und Verbindungen
 **/
 pub fn cities() -> Vec<(usize, City, &'static str)> {
    vec![
        (1, City::Basel, "Basel,+Schweiz"),
        (2, City::Berlin, "Berlin"),
        (3, City::Dortmund, "Dortmund"),
        (4, City::Dresden, "Dresden"),
        (5, City::Frankfurt, "Frankfurt"),
        (6, City::Hamburg, "Hamburg"),
        (7, City::Muenchen, "M%C3%BCnchen"),
        (8, City::Potsdam, "Potsdam"),
        (9, City::Strassburg, "Stra%C3%9Fburg,+Frankreich"),   
    ]
}

pub fn driven(chromosome: &[usize]) -> usize {
    let routes = routes();
    let mut distance = 0usize;
    for (i, gene) in chromosome.iter().enumerate() {
        let from =  city(*gene);
        
        if i < chromosome.len() - 1 {
            let to = city(chromosome[i + 1]);
            
            for c in &routes {
                if c.0 == from && c.1 == to {
                    let current_dist = c.2;
                    distance += current_dist;
                }
            }
        } else {
            let to = city(chromosome[0]);
            for c in &routes {
                if c.0 == from && c.1 == to {
                    let current_dist = c.2;
                    distance += current_dist;
                }
            }

        }      
    }
    distance
}


pub fn min_max(evaluated:  &[(Vec<usize>, usize)]) -> (usize, usize) {
    let mut routes = vec![];
    for chromosome in evaluated {
        routes.push(chromosome.1);
    }
    routes.sort_unstable();
    ( routes[0] , routes[routes.len() - 1] )
}

